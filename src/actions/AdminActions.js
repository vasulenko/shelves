import {
  ADD_NEW_AUTHOR,
  ADD_NEW_BOOK,
  ADD_NEW_PUBLISHER,
  ADD_NEW_SHELVE,
  ADD_NEW_SUBSCRIPTION,
  UPDATE_AUTHOR,
  UPDATE_BOOK,
  UPDATE_PUBLISHER,
  UPDATE_SHELVE,
  UPDATE_SUBSCRIPTION,
  DELETE_AUTHOR,
  DELETE_BOOK,
  DELETE_PUBLISHER,
  DELETE_SHELVE,
  DELETE_SUBSCRIPTION
} from "../constants/Admin";
import {API_URL} from "../constants/Library";

export function addNewBook(payload) {
  return (dispatch, getState) => {
    const token = getState().user.token;
    let xhr = new XMLHttpRequest();
    let url = API_URL + 'upload/thumbnail/';
    xhr.open('POST', url, false);
    xhr.setRequestHeader('Authorization', 'Bearer ' + token);
    let formData = new FormData();
    formData.append('file', payload.thumbnail);
    console.log(formData);
    xhr.send(formData);
    let res = JSON.parse(xhr.response);

    let xhr1 = new XMLHttpRequest();
    url = API_URL + 'upload/book/';
    formData = new FormData();
    formData.append('file', payload.file);
    xhr1.open('POST', url, false);
    xhr1.setRequestHeader('Authorization', 'Bearer ' + token);
    xhr1.send(formData);

    let sRes = JSON.parse(xhr1.response);

    let xhr2 = new XMLHttpRequest();
    url = API_URL + 'book/';
    xhr2.open('POST', url, false);
    xhr2.setRequestHeader('Authorization', 'Bearer ' + token);
    xhr2.setRequestHeader('Content-Type', 'application/json');
    xhr2.send(JSON.stringify({
      name: payload.name,
      description: payload.description,
      isbn: payload.isbn,
      publisherId: payload.pubId,
      shelveId: payload.shelveId,
      authorId: payload.authorId,
      price: isNaN(parseInt(payload.price)) ? 10 : parseInt(payload.price),
      thumbnailLink: res.link,
      partFileLink: sRes.partLink,
      fileName: sRes.blobName
    }));

    let finallRes = JSON.parse(xhr2.response);
    console.log(finallRes);
    dispatch({
      type: ADD_NEW_BOOK,
      payload: {}
    })
  }
}

export function addNewSubscription(data) {
  return (dispatch, getState) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'subscription/type/';
    xhr.open('POST', url, false);
    xhr.setRequestHeader('Authorization', 'Bearer ' + getState().user.token);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(data));
    console.log(JSON.parse(xhr.response));
    dispatch({
      type: ADD_NEW_SUBSCRIPTION,
      payload: {}
    })
  }
}

export function addNewPublisher(data) {
  return (dispatch, getState) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'publisher/';
    xhr.open('POST', url, false);
    xhr.setRequestHeader('Authorization', 'Bearer ' + getState().user.token);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(data));
    console.log(JSON.parse(xhr.response));
    dispatch({
      type: ADD_NEW_PUBLISHER,
      payload: {}
    })
  }
}

export function addNewShelve(data) {
  return (dispatch, getState) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'shelve/';
    xhr.open('POST', url, false);
    xhr.setRequestHeader('Authorization', 'Bearer ' + getState().user.token);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(data));
    console.log(JSON.parse(xhr.response));
    dispatch({
      type: ADD_NEW_SHELVE,
      payload: {}
    })
  }
}

export function addNewAuthor(data) {
  return (dispatch, getState) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'author/';
    xhr.open('POST', url, false);
    xhr.setRequestHeader('Authorization', 'Bearer ' + getState().user.token);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(data));
    console.log(JSON.parse(xhr.response));
    dispatch({
      type: ADD_NEW_AUTHOR,
      payload: {}
    })
  }
}

export function updateBook(data) {
  let xhr = new XMLHttpRequest();
  const url = API_URL + 'book/' + data.id + '/';
  xhr.open('PUT', url, false);
  xhr.setRequestHeader('Authorization', 'Bearer ' + window.localStorage.getItem('rr_token'));
  xhr.setRequestHeader('Content-Type', 'application/json');
  delete data.id;
  xhr.send(JSON.stringify(data));
  console.log(xhr.response);
}

export function deleteBook(id) {
  let xhr = new XMLHttpRequest();
  const url = API_URL + 'book/' + id + '/';
  xhr.open('DELETE', url, false);
  xhr.setRequestHeader('Authorization', 'Bearer ' + window.localStorage.getItem('rr_token'));
  xhr.send();
  console.log(xhr.response);
}

export function updateAuthor(data) {
  let xhr = new XMLHttpRequest();
  const url = API_URL + 'author/' + data.id + '/';
  xhr.open('PUT', url, false);
  xhr.setRequestHeader('Authorization', 'Bearer ' + window.localStorage.getItem('rr_token'));
  xhr.setRequestHeader('Content-Type', 'application/json');
  delete data.id;
  xhr.send(JSON.stringify(data));
  console.log(xhr.response);
}

export function deleteAuthor(id) {
  let xhr = new XMLHttpRequest();
  const url = API_URL + 'author/' + id + '/';
  xhr.open('DELETE', url, false);
  xhr.setRequestHeader('Authorization', 'Bearer ' + window.localStorage.getItem('rr_token'));
  xhr.send();
  console.log(xhr.response);
}

export function updateSubscription(data) {
  let xhr = new XMLHttpRequest();
  const url = API_URL + 'subscription/type/' + data.id + '/';
  xhr.open('PUT', url, false);
  xhr.setRequestHeader('Authorization', 'Bearer ' + window.localStorage.getItem('rr_token'));
  xhr.setRequestHeader('Content-Type', 'application/json');
  delete data.id;
  xhr.send(JSON.stringify(data));
  console.log(xhr.response);
}

export function deleteSubscription(id) {
  let xhr = new XMLHttpRequest();
  const url = API_URL + 'subscription/type/' + id + '/';
  xhr.open('DELETE', url, false);
  xhr.setRequestHeader('Authorization', 'Bearer ' + window.localStorage.getItem('rr_token'));
  xhr.send();
  console.log(xhr.response);
}

export function updatePublisher(data) {
  let xhr = new XMLHttpRequest();
  const url = API_URL + 'publisher/' + data.id + '/';
  xhr.open('PUT', url, false);
  xhr.setRequestHeader('Authorization', 'Bearer ' + window.localStorage.getItem('rr_token'));
  xhr.setRequestHeader('Content-Type', 'application/json');
  delete data.id;
  xhr.send(JSON.stringify(data));
  console.log(xhr.response);
}

export function deletePublisher(id) {
  let xhr = new XMLHttpRequest();
  const url = API_URL + 'publisher/' + id + '/';
  xhr.open('DELETE', url, false);
  xhr.setRequestHeader('Authorization', 'Bearer ' + window.localStorage.getItem('rr_token'));
  xhr.send();
  console.log(xhr.response);
}

export function updateShelve(data) {
  let xhr = new XMLHttpRequest();
  const url = API_URL + 'shelve/' + data.id + '/';
  xhr.open('PUT', url, false);
  xhr.setRequestHeader('Authorization', 'Bearer ' + window.localStorage.getItem('rr_token'));
  xhr.setRequestHeader('Content-Type', 'application/json');
  delete data.id;
  xhr.send(JSON.stringify(data));
  console.log(xhr.response);
}

export function deleteShelve(id) {
  let xhr = new XMLHttpRequest();
  const url = API_URL + 'shelve/' + id;
  xhr.open('DELETE', url, false);
  xhr.setRequestHeader('Authorization', 'Bearer ' + window.localStorage.getItem('rr_token'));
  xhr.send();
  console.log(xhr.response);
}