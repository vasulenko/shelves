import {REMOVE_FROM_CART, ADD_TO_CART, API_URL, REPORT} from '../constants/Library';
import {FETCH_ALL_BOOKS, FETCH_BOOK_BY_ID, SEARCH_BOOK} from "../constants/Book";
import {FETCH_ALL_AUTHORS, FETCH_AUTHOR_BY_ID} from "../constants/Author";
import {
  FETCH_ALL_SHELVES,
  FETCH_SHELVE_BY_ID,
  FETCH_SHELVE_NEW,
  FETCH_SHELVE_TOP
} from "../constants/Shelves";
import {FETCH_ALL_PUBLISHERS, FETCH_PUBLISHER_BY_ID} from "../constants/Publisher";
import {FETCH_ALL_SUBSCRIPTION, FETCH_SUBSCRIPTION_BY_ID} from "../constants/Subscription";
import {ORDER_INIT, ORDER_PAY} from "../constants/Order";
import {browserHistory} from "react-router";

export function downloadBookById(id) {
  const xhr = new XMLHttpRequest();
  const url = API_URL + 'download/book/' + id + '/';
  xhr.open("GET", url, false);
  xhr.setRequestHeader("Authorization", "Bearer " + window.localStorage.getItem('rr_token'));
  xhr.send();
  console.log(xhr.response);
  let a = document.createElement('a');
  a.href = JSON.parse(xhr.responseText).link;
  a.download = id;
  a.style.display = 'none';
  a.id = "tmp_link";
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(document.getElementById('tmp_link'));
}

export function getBookById(id) {
  let xhr = new XMLHttpRequest();
  const url = API_URL + 'book/' + id;
  xhr.open('GET', url, false);
  xhr.send();
  return JSON.parse(xhr.response);
}

export function addToCart(id) {
  return (dispatch, getState) => {
    const booksId = getState().library.cart;
    booksId.push(id);
    dispatch({
      type: ADD_TO_CART,
      payload: booksId
    });
  }
}

export function removeFromCart(id) {
  return (dispatch, getState) => {
    let booksId = getState().library.cart;
    booksId = booksId.filter(el => {
      return el.id === id;
    });
    dispatch({
      type: REMOVE_FROM_CART,
      payload: booksId
    });
  }
}

export function fetchAllBooks() {
  return (dispatch) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'book/all';
    xhr.open('GET', url, false);
    xhr.send();
    let books = JSON.parse(xhr.response);
    dispatch({
      type: FETCH_ALL_BOOKS,
      payload: books
    });
  }
}

export function searchBook(data) {
  return (dispatch) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'book/all?query=' + data;
    xhr.open('GET', url, false);
    xhr.send();

    dispatch({
      type: SEARCH_BOOK,
      payload: JSON.parse(xhr.response)
    });
  }
}

export function fetchBookById(id) {
  return (dispatch) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'book/' + id;
    xhr.open('GET', url, false);
    xhr.send();
    let book = JSON.parse(xhr.response);
    dispatch({
      type: FETCH_BOOK_BY_ID,
      payload: book
    });
  }
}

export function fetchAllSubscriptions() {
  return (dispatch) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'subscription/type/';
    xhr.open('GET', url, false);
    xhr.send();
    let subscriptions = JSON.parse(xhr.response);
    dispatch({
      type: FETCH_ALL_SUBSCRIPTION,
      payload: subscriptions
    });
  }
}

export function fetchSubscriptionById(id) {
  return (dispatch) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'subscription/' + id;
    xhr.open('GET', url, false);
    xhr.send();
    let subscription = JSON.parse(xhr.response);
    dispatch({
      type: FETCH_SUBSCRIPTION_BY_ID,
      payload: subscription
    });
  }
}

export function fetchAllAuthor() {
  return (dispatch) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'author/';
    xhr.open('GET', url, false);
    xhr.send();
    let authors = JSON.parse(xhr.response);
    dispatch({
      type: FETCH_ALL_AUTHORS,
      payload: authors
    });
  }
}

export function fetchAuthorById(id) {
  return (dispatch) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'book/' + id;
    xhr.open('GET', url, false);
    xhr.send();
    let author = JSON.parse(xhr.response);
    dispatch({
      type: FETCH_AUTHOR_BY_ID,
      payload: author
    });
  }
}

export function fetchAllShelves() {
  return (dispatch) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'shelve/';
    xhr.open('GET', url, false);
    xhr.send();
    let authors = JSON.parse(xhr.response);
    dispatch({
      type: FETCH_ALL_SHELVES,
      payload: authors
    });
  }
}

export function fetchShelveById(id) {
  return (dispatch) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'shelve/' + id;
    xhr.open('GET', url, false);
    xhr.send();
    let shelve = JSON.parse(xhr.response);
    dispatch({
      type: FETCH_SHELVE_BY_ID,
      payload: shelve
    });
  }
}

export function fetchShelveTop() {
  return (dispatch) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'offers/top';
    xhr.open('GET', url, false);
    xhr.send();
    let top = JSON.parse(xhr.response);
    dispatch({
      type: FETCH_SHELVE_TOP,
      payload: top
    });
  }
}

export function fetchShelveNew() {
  return (dispatch) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'offers/new';
    xhr.open('GET', url, false);
    xhr.send();
    let newBooks = JSON.parse(xhr.response);
    dispatch({
      type: FETCH_SHELVE_NEW,
      payload: newBooks
    });
  }
}

export function fetchAllPublisher() {
  return (dispatch) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'publisher/';
    xhr.open('GET', url, false);
    xhr.send();
    let authors = JSON.parse(xhr.response);
    dispatch({
      type: FETCH_ALL_PUBLISHERS,
      payload: authors
    });
  }
}

export function fetchPublisherById(id) {
  return (dispatch) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'publisher/' + id;
    xhr.open('GET', url, false);
    xhr.send();
    let book = JSON.parse(xhr.response);
    dispatch({
      type: FETCH_PUBLISHER_BY_ID,
      payload: book
    });
  }
}

export function fetchReport(start, end, type, helper = '') {
  return (dispatch, getState) => {
    console.log(start);
    console.log(end);
    let xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    const url = API_URL + `report/sales/${type}?${helper !== '' ? type + 'Id=' + helper + '&' : ''}since=${start}&until=${end}`;
    xhr.open('GET', url, false);
    xhr.setRequestHeader('Authorization', 'Bearer ' + getState().user.token);
    xhr.send();
    console.log(xhr.response);


    dispatch({
      type: REPORT,
      payload: JSON.parse(xhr.responseText)
    });
  }
}

export function orderInit(ids) {
  return (dispatch, getState) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'order/open/';
    xhr.open('POST', url, false);
    console.log(getState().user.token);
    xhr.setRequestHeader('Authorization', 'Bearer ' + getState().user.token);
    xhr.setRequestHeader('Content-Type', 'application/json');
    console.log(JSON.stringify({
      'booksIds': ids
    }));
    xhr.send(JSON.stringify({
      'booksIds': ids
    }));
    let res = JSON.parse(xhr.responseText);
    console.log(xhr.response);
    browserHistory.push('/payment/');
    dispatch({
      type: ORDER_INIT,
      payload: {
        price: res.price,
        id: res.id
      }
    });
  }
}

export function orderPay(data) {
  return (dispatch, getState) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + `order/${data.id}/pay/`;
    xhr.open('POST', url, false);
    console.log(getState().user.token);
    xhr.setRequestHeader('Authorization', 'Bearer ' + getState().user.token);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
      'token': data.number
    }));
    console.log(xhr.response);
    if (xhr.status === 200) {
      browserHistory.push('/');
      dispatch({
        type: ORDER_PAY,
        payload: {}
      });
    }
  }
}

export function getShelveById(id) {
  let xhr = new XMLHttpRequest();
  const url = API_URL + 'shelve/' + id;
  xhr.open('GET', url, false);
  xhr.send();
  return JSON.parse(xhr.response);
}

export function getPublisherById(id) {
  let xhr = new XMLHttpRequest();
  const url = API_URL + 'publisher/' + id;
  xhr.open('GET', url, false);
  xhr.send();
  return JSON.parse(xhr.response);
}

export function getAuthorById(id) {
  let xhr = new XMLHttpRequest();
  const url = API_URL + 'author/' + id;
  xhr.open('GET', url, false);
  xhr.send();
  return JSON.parse(xhr.response);
}

export function getSubscriptionById(id) {
  let xhr = new XMLHttpRequest();
  const url = API_URL + 'subscription/' + id;
  xhr.open('GET', url, false);
  xhr.send();
  return JSON.parse(xhr.response);
}

export function fetchReportToFile(start, end, type, helper = '') {
  let xhr = new XMLHttpRequest();
  const url = API_URL + `report/sales/${type}/csv?${helper !== '' ? type + 'Id=' + helper + '&' : ''}since=${start}&until=${end}`;
  xhr.open('GET', url, false);
  xhr.setRequestHeader('Authorization', 'Bearer ' + window.localStorage.getItem('rr_token'));
  xhr.send();
  let a = document.createElement('a');
  a.href = "data:text/csv;charset=utf-8," + xhr.responseText;
  a.download = 'Report.csv';
  a.style.display = 'none';
  a.id = "tmp_link";
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(document.getElementById('tmp_link'));
  console.log(xhr.responseText);
  return xhr.responseText
}