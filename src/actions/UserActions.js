import {
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  SIGNUP_FAIL,
  SIGNUP_SUCCESS,
  LOGOUT,
  FETCH_USER,
  FETCH_USER_BOOKS
} from '../constants/User';
import {API_URL} from "../constants/Library";
import {browserHistory} from 'react-router';

export function checkCookie() {
  return (dispatch) => {
    const token = window.localStorage.getItem('rr_token') || '';
    const name = window.localStorage.getItem('rr_name') || '';
    const role = window.localStorage.getItem('rr_role') || '';
    if (token !== '') {
      dispatch({
        type: LOGIN_SUCCESS,
        payload: {
          name: name,
          token: token,
          role: role,
          isAuthenticated: true,
          books: []
        }
      })
    }
  }
}

export function fetchUser() {
  return (dispatch, getState) => {
    const xhr1 = new XMLHttpRequest();
    const url = API_URL + 'user/me/';
    xhr1.open("GET", url, false);
    xhr1.setRequestHeader('Authorization', 'Bearer ' + getState().user.token);
    xhr1.send();
    let res = JSON.parse(xhr1.response);
    dispatch({
      type: FETCH_USER,
      payload: {
        name: res.name,
        id: res.id,
        bookAvailable: res.bookAvailable
      }
    })
  }
}

export function fetchUserBooks() {
  return (dispatch, getState) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'user/me/books/';
    xhr.open('GET', url, false);
    xhr.setRequestHeader('Authorization', 'Bearer ' + getState().user.token);
    xhr.send();
    let res = JSON.parse(xhr.response);
    console.log(res);
    dispatch({
      type: FETCH_USER_BOOKS,
      payload: {
        books: res['_embedded']
      }
    })
  }
}

export function login(payload) {
  return (dispatch) => {
    let xhr = new XMLHttpRequest();
    let url = API_URL + 'auth/signin?email=' + payload.email + '&password=' + payload.password;
    xhr.open('POST', url, false);
    xhr.send();
    const result = JSON.parse(xhr.response);

    browserHistory.push('/');

    window.localStorage.setItem('rr_token', result.token);
    window.localStorage.setItem('rr_name', result.name);
    const role = result.authorities.pop();
    window.localStorage.setItem('rr_role', role);

    xhr = new XMLHttpRequest();
    url = API_URL + 'user/me/';
    xhr.open('GET', url, false);
    xhr.setRequestHeader('Authorization', 'Bearer ' + result.token);
    xhr.send();
    let res = JSON.parse(xhr.response);

    dispatch({
      type: LOGIN_SUCCESS,
      payload: {
        name: result.name,
        token: result.token,
        role: role,
        bookAvailable: res.bookAvailable,
        isAuthenticated: true,
        books: []
      }
    })
  }
}


export function logout() {
  return (dispatch) => {
    window.localStorage.setItem('rr_token', '');
    window.localStorage.setItem('rr_name', '');
    browserHistory.push('/');
    dispatch({
      type: LOGOUT,
      payload: {
        name: '',
        token: '',
        isAuthenticated: false
      }
    })
  }
}

export function signup(payload) {
  return (dispatch) => {
    let xhr = new XMLHttpRequest();
    const url = API_URL + 'auth/signup?email=' + payload.email + '&password=' + payload.password + '&name=' + payload.name;
    xhr.open('POST', url, false);
    xhr.send();
    const result = xhr.responseText;
    const role = result.authorities.pop();

    if (result) {
      dispatch({
        type: SIGNUP_SUCCESS,
        payload: {
          token: result.token,
          role: role,
          bookAvailable: result.bookAvailable,
          isAuthenticated: true
        }
      })
    }

    else {
      dispatch({
        type: SIGNUP_FAIL,
        payload: {}
      })
    }
  };
}