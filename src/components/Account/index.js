import React, {Component} from 'react';
import '../Account/style.css';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as LibraryActions from "../../actions/LibraryActions";
import * as UserActions from "../../actions/UserActions";
import NavLink from '../NavLink';

export class Account extends Component {
  componentWillMount() {
    this.props.actions.fetchUser();
    this.props.actions.fetchUserBooks();
  }


  logout() {
    this.props.actions.logout();
  }

  render() {
    const books = this.props.user.books || [];

    const booksArr = books.map(book => {
      return (
        <div className='block--shelve-book' key={book.link} style={{'backgroundImage': `url(${book.thumbnailLink})`}}>
          <div className='block--shelve-book-hover'>
            <span>{book.name}</span>
            <NavLink to={'/book/' + book.id}>More</NavLink>
          </div>
        </div>
      )
    });
    return (
      <div className='block--account'>
        <button className={'btn red accent-2'} onClick={this.logout.bind(this)}>Logout</button>
        <div className='block--account-books'>
          {booksArr}
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    library: state.library
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({...LibraryActions, ...UserActions}, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Account)