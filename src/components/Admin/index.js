import React, {Component} from 'react';
import '../Admin/style.css';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as AdminActions from "../../actions/AdminActions";
import * as UserActions from "../../actions/UserActions";
import NavLink from '../../components/NavLink';

export class Admin extends Component {
  addBook(e) {
    e.preventDefault();
    const name = document.getElementById('book-name').value;
    const description = document.getElementById('book-description').value;
    const isbn = document.getElementById('book-isbn').value;
    const pubId = document.getElementById('book-pubId').value;
    const shelveId = document.getElementById('book-shelveId').value;
    const price = document.getElementById('book-price').value;
    const authorId = document.getElementById('book-authorId').value;
    const thumbnail = document.getElementById('book-thumbnail').files[0];
    const file = document.getElementById('book-file').files[0];
    const data = {
      name: name,
      description: description,
      isbn: isbn,
      pubId: pubId,
      shelveId: shelveId,
      price: price,
      authorId: authorId,
      thumbnail: thumbnail,
      file: file
    };
    this.props.actions.addNewBook(data);
  }

  initSelect() {
    this.props.library.publisher.map(el => {
      let option = document.createElement('option');
      option.value = el.id;
      option.innerText = el.name;
      document.getElementById('publisher_select').appendChild(option);
    });

    this.props.library.books.map(el => {
      let option = document.createElement('option');
      option.value = el.id;
      option.innerText = el.name;
      document.getElementById('book_select').appendChild(option);
    });

    this.props.library.shelves.map(el => {
      let option = document.createElement('option');
      option.value = el.id;
      option.innerText = el.name;
      document.getElementById('shelve_select').appendChild(option);
    });

    this.props.library.authors.map(el => {
      let option = document.createElement('option');
      option.value = el.id;
      option.innerText = el.name;
      document.getElementById('author_select').appendChild(option);
    });

    this.props.library.subscriptions.map(el => {
      let option = document.createElement('option');
      option.value = el.id;
      option.innerText = el.id;
      document.getElementById('subscription_select').appendChild(option);
    });
  }

  componentDidMount() {
    this.initSelect();
  }

  addSubscriptionType(e) {
    e.preventDefault();
    const name = document.getElementById('subscription-name').value;
    const count = document.getElementById('subscription-count').value;
    const price = document.getElementById('subscription-price').value;
    const data = {
      name: name,
      maxBookPrice: price,
      count: count
    };
    console.log(data);
    this.props.actions.addNewSubscription(data);
  }

  addPublisher(e) {
    e.preventDefault();
    const name = document.getElementById('publisher-name').value;
    const data = {
      name: name
    };
    console.log(data);
    this.props.actions.addNewPublisher(data);
  }

  addShelve(e) {
    e.preventDefault();
    const name = document.getElementById('shelve-name').value;
    const data = {
      name: name
    };
    console.log(data);
    this.props.actions.addNewShelve(data);
  }

  addAuthor(e) {
    e.preventDefault();
    const name = document.getElementById('author-name').value;
    const data = {
      name: name
    };
    console.log(data);
    this.props.actions.addNewAuthor(data);
  }

  logout() {
    this.props.actions.logout();
  }

  updateBook() {

  }

  deleteBook() {
    const id = document.getElementById('book_select').value;

    AdminActions.deleteBook(id);
  }

  updateAuthor() {
    const id = document.getElementById('author_select').value;
    const name = document.getElementById('author-name-update').value;
    const data = {id: id, name: name};
    console.log(data);
    AdminActions.updateAuthor(data);
  }

  deleteAuthor() {
    const id = document.getElementById('author_select').value;
    AdminActions.deleteAuthor(id);
  }

  updateSubscription() {
    const id = document.getElementById('subscription_select').value;
    const name = document.getElementById('subscription-name-update').value;
    const price = document.getElementById('subscription-price-update').value;
    const count = document.getElementById('subscription-count-update').value;
    const data = {id: id, name: name, count: count, maxBookPrice: price};
    console.log(data);
    AdminActions.updateSubscription(data);
  }

  deleteSubscription() {
    const id = document.getElementById('subscription_select').value;
    AdminActions.deleteSubscription(id);
  }

  updatePublisher() {
    const id = document.getElementById('publisher_select').value;
    const name = document.getElementById('publisher-name-update').value;
    const data = {id: id, name: name};
    console.log(data);
    AdminActions.updatePublisher(data);
  }

  deletePublisher() {
    const id = document.getElementById('publisher_select').value;
    AdminActions.deletePublisher(id);
  }

  updateShelve() {
    const id = document.getElementById('shelve_select').value;
    const name = document.getElementById('shelve-name-update').value;
    const data = {id: id, name: name};
    console.log(data);
    AdminActions.updateShelve(data);
  }

  deleteShelve() {
    const id = document.getElementById('shelve_select').value;
    AdminActions.deleteShelve(id);
  }

  render() {
    return (
      <div className='block--admin'>
        <NavLink to='/report' className={'btn red accent-2'}>Report Page</NavLink>
        <button className={'btn red accent-2'} onClick={this.logout.bind(this)}>Logout</button>
        <form className={'form'} name='addBook' onSubmit={this.addBook.bind(this)}>
          <h2 className='block--admin-form-title'>Add new book</h2>
          <div className="input-field">
            <input id="book-name" placeholder={'Book name'} required type={'text'} className="validate"/>
          </div>
          <div className="input-field">
            <textarea id="book-description" required placeholder={'Book description'} className="validate"/>
          </div>
          <div className={"input-field"}>
            <input id="book-isbn" placeholder={'ISBN'} required className="validate"/>
          </div>
          <div className={"input-field"}>
            <input id="book-pubId" placeholder={'Publisher Id'} required className="validate"/>
          </div>
          <div className={"input-field"}>
            <input id="book-shelveId" placeholder={'Shelve Id'} required className="validate"/>
          </div>
          <div className={"input-field"}>
            <input id="book-price" placeholder={'Book Price'} required className="validate"/>
          </div>
          <div className={"input-field"}>
            <input id="book-authorId" placeholder={'Author Id'} required className="validate"/>
          </div>
          <div className={"input-field"}>
            <span>Thumbnail</span><input id="book-thumbnail" required type='file' placeholder={'Thumbnail'}
                                         className="validate"/>
          </div>
          <div className={"input-field"}>
            <span>Book</span> <input id="book-file" type='file' required placeholder={'File'} accept='.pdf'
                                     className="validate"/>
          </div>

          <button className={'btn btn-primary red accent-2'} type='submit'>Done</button>
        </form>

        <form className={'form'} name='addPublisher' onSubmit={this.addPublisher.bind(this)}>
          <h2 className='block--admin-form-title'>Add new publisher</h2>
          <div className="input-field">
            <input id="publisher-name" placeholder={'Name'} required type={'text'} className="validate"/>
          </div>
          <button className={'btn btn-primary red accent-2'} type='submit'>Done</button>
        </form>

        <form className={'form'} name='addAuthor' onSubmit={this.addAuthor.bind(this)}>
          <h2 className='block--admin-form-title'>Add new author</h2>
          <div className="input-field">
            <input id="author-name" placeholder={'Author name'} required type={'text'} className="validate"/>
          </div>
          <button className={'btn btn-primary red accent-2'} type='submit'>Done</button>
        </form>

        <form className={'form'} name='addShelve' onSubmit={this.addShelve.bind(this)}>
          <h2 className='block--admin-form-title'>Add new shelve</h2>
          <div className="input-field">
            <input id="shelve-name" placeholder={'Shelve name'} required type={'text'} className="validate"/>
          </div>
          <button className={'btn btn-primary red accent-2'} type='submit'>Done</button>
        </form>

        <form className={'form'} name='addSubscription' onSubmit={this.addSubscriptionType.bind(this)}>
          <h2 className='block--admin-form-title'>Add new subscription type</h2>
          <div className="input-field">
            <input id="subscription-name" required placeholder={'Subscription name'} type={'text'}
                   className="validate"/>
          </div>
          <div className="input-field">
            <input id="subscription-count" required placeholder={'Count of book in subscription'} type={'text'}
                   className="validate"/>
          </div>
          <div className="input-field">
            <input id="subscription-price" required placeholder={'Max book price'} type={'text'} className="validate"/>
          </div>
          <button className={'btn btn-primary red accent-2'} type='submit'>Done</button>
        </form>

        <div className={'form'}>
          <h2 className='block--admin-form-title'>Manage subscription type</h2>
          <select name="subscription_select" id="subscription_select" className="browser-default"></select>
          <div className="input-field">
            <input id="subscription-name-update" required placeholder={'Subscription name'} type={'text'}
                   className="validate"/>
          </div>
          <div className="input-field">
            <input id="subscription-count-update" required placeholder={'Count of book in subscription'} type={'text'}
                   className="validate"/>
          </div>
          <div className="input-field">
            <input id="subscription-price-update" required placeholder={'Max book price'} type={'text'}
                   className="validate"/>
          </div>
          <button className={'btn btn-primary red accent-2'} onClick={this.updateSubscription.bind(this)}>Done</button>
          <button className={'btn btn-primary red accent-2'} onClick={this.deleteSubscription.bind(this)}>Delete
          </button>
        </div>

        <div className={'form'}>
          <h2 className='block--admin-form-title'>Manage author type</h2>
          <select name="author_select" id="author_select" className="browser-default"></select>
          <div className="input-field">
            <input id="author-name-update" required placeholder={'Author name'} type={'text'}
                   className="validate"/>
          </div>
          <button className={'btn btn-primary red accent-2'} onClick={this.updateAuthor.bind(this)}>Done</button>
          <button className={'btn btn-primary red accent-2'} onClick={this.deleteAuthor.bind(this)}>Delete</button>
        </div>

        <div className={'form'}>
          <h2 className='block--admin-form-title'>Manage shelve type</h2>
          <select name="shelve_select" id="shelve_select" className="browser-default"></select>
          <div className="input-field">
            <input id="shelve-name-update" required placeholder={'Shelve name'} type={'text'}
                   className="validate"/>
          </div>
          <button className={'btn btn-primary red accent-2'} onClick={this.updateShelve.bind(this)}>Done</button>
          <button className={'btn btn-primary red accent-2'} onClick={this.deleteShelve.bind(this)}>Delete</button>
        </div>

        <div className={'form'}>
          <h2 className='block--admin-form-title'>Manage publisher type</h2>
          <select name="publisher_select" id="publisher_select" className="browser-default"></select>
          <div className="input-field">
            <input id="publisher-name-update" required placeholder={'Publisher name'} type={'text'}
                   className="validate"/>
          </div>
          <button className={'btn btn-primary red accent-2'} onClick={this.updatePublisher.bind(this)}>Done</button>
          <button className={'btn btn-primary red accent-2'} onClick={this.deletePublisher.bind(this)}>Delete</button>
        </div>
        <div className={'form'}>
          <h2 className='block--admin-form-title'>Manage book</h2>
          <select name="book_select" id="book_select" className="browser-default"></select>
          <div className="input-field">
            <input id="book-name-update" placeholder={'Book name'} required type={'text'} className="validate"/>
          </div>
          <div className="input-field">
            <textarea id="book-description-update" required placeholder={'Book description'} className="validate"/>
          </div>
          <div className={"input-field"}>
            <input id="book-isbn-update" placeholder={'ISBN'} required className="validate"/>
          </div>
          <div className={"input-field"}>
            <input id="book-pubId-update" placeholder={'Publisher Id'} required className="validate"/>
          </div>
          <div className={"input-field"}>
            <input id="book-shelveId-update" placeholder={'Shelve Id'} required className="validate"/>
          </div>
          <div className={"input-field"}>
            <input id="book-price-update" placeholder={'Book Price'} required className="validate"/>
          </div>
          <div className={"input-field"}>
            <input id="book-authorId-update" placeholder={'Author Id'} required className="validate"/>
          </div>
          <div className={"input-field"}>
            <span>Thumbnail</span><input id="book-thumbnail" required type='file' placeholder={'Thumbnail'}
                                         className="validate"/>
          </div>
          <div className={"input-field"}>
            <span>Book</span> <input id="book-file" type='file' required placeholder={'File'} accept='.pdf'
                                     className="validate"/>
          </div>

          <button className={'btn btn-primary red accent-2'} onClick={this.updateBook.bind(this)}>Done</button>
          <button className={'btn btn-primary red accent-2'} onClick={this.deleteBook.bind(this)}>Delete</button>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    library: state.library
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({...AdminActions, ...UserActions}, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Admin)
