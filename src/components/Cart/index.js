import React, {Component} from 'react';
import '../Cart/style.css';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as LibraryActions from "../../actions/LibraryActions";


export class Cart extends Component {
  componentWillMount() {
  }

  payload() {
    this.props.actions.orderInit(this.props.library.cart);
  }

  render() {
    const booksId = this.props.library.cart;
    let books = [];
    booksId.forEach(id => {
      books.push(LibraryActions.getBookById(id));
    });
    let total = 0;
    const booksArr = books.map(book => {
      total += book.price;
      return (
        <div className='block--cart-book'>
          <img src={book.thumbnailLink} alt="book"/>
          <div className='block--cart-book-content'>
            <h2 className='block--cart-book-title'>
              {book.name}
            </h2>
            <h2 className='block--cart-book-price'>
              Price: ${book.price}
            </h2>
          </div>
        </div>
      )
    });
    return (
      <div className='block--cart'>
        {booksArr}
        {total !== 0 ? <div className='block--cart-total'>
          Total: {total}
          <button onClick={this.payload.bind(this)} className={'btn red accent-2'}>Rush</button>
        </div> : ''}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    library: state.library
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(LibraryActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart)