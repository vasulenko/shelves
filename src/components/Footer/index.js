import React, {Component} from 'react';
import NavLink from '../NavLink';
import '../Footer/style.css';

export default class Footer extends Component {
  render() {
    return (
      <footer className={'block--footer'}>
        <nav className={'nav red accent-2'}>
          <div className={"nav-item"}>
            <NavLink to='/'>Home</NavLink>
          </div>
          <div className={"nav-item"}>
            <NavLink to='/shelves'>Shelves</NavLink>
          </div>
          <div className={"nav-item"}>
            <NavLink to='/search'>Search</NavLink>
          </div>
          <div className={"nav-item"}>
            <NavLink to='/subscribes'>Subscriptions</NavLink>
          </div>
          <div className={"nav-item"}>
            <NavLink to='/cart'>Cart</NavLink>
          </div>
          <div className={"nav-item"}>
            <NavLink to='/me'>Account</NavLink>
          </div>
        </nav>
        <div className="block--footer-description">
          V & P Group@ 2017 All Rights Reserved
        </div>
      </footer>
    )
  }
}
