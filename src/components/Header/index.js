import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import NavLink from '../NavLink';
import '../Header/style.css';
import * as UserActions from "../../actions/UserActions";

export class Header extends Component {
  componentWillMount() {

  }

  render() {
    const role = this.props.user.role;
    return (
      <header className={'block--header'}>
        <nav className={'nav red accent-2'}>
          <div className="nav-item">
            <img src="/images/logo.png" alt=""/>
          </div>
          <div className={"nav-item"}>
            <NavLink to='/' className={'btn red accent-2'}>Home</NavLink>
          </div>
          <div className={"nav-item"}>
            <NavLink to='/shelves' className={'btn red accent-2'}>Shelves</NavLink>
          </div>
          <div className={"nav-item"}>
            <NavLink to='/search' className={'btn red accent-2'}>Search</NavLink>
          </div>
          <div className={"nav-item"}>
            <NavLink to='/subscribe' className={'btn red accent-2'}>Subscriptions</NavLink>
          </div>
          <div className={"nav-item"}>
            <NavLink to='/cart' className={'btn red accent-2'}>Cart</NavLink>
          </div>
          {this.props.user.token
            ? <div className={"nav-item"}>
              {role === "ROLE_ADMIN" ?
                <NavLink to='/admin' className={'btn red accent-2'}>{this.props.user.name}</NavLink>
                : <NavLink to='/me' className={'btn red accent-2'}>{this.props.user.name}</NavLink>}
            </div>
            : <div className={"nav-item"}>
              <a className={'dropdown-button btn red accent-2'} href={''} data-activates='dropdown1'>Sign</a>
              <ul id='dropdown1' className={'dropdown-content'}>
                <li><NavLink to='/sign-in'>Sign In</NavLink></li>
                <li><NavLink to='/sign-up'>Sign Up</NavLink></li>
              </ul>
            </div>}

        </nav>
      </header>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    library: state.library
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(UserActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)