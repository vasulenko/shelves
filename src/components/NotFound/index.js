import React, {Component} from 'react';
import {Link} from 'react-router';
import '../NotFound/style.css';

export default class NotFound extends Component {
  render() {
    return (
      <div className='block--not-found'>
        <h1>404</h1>
        <Link to='/'>BACK</Link>
      </div>
    )
  }
}
