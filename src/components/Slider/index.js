import React, {Component} from 'react';
import '../Slider/style.css';
import NavLink from '../NavLink';

export default class Slider extends Component {
  render() {
    const books = this.props.books || [];
    console.log(books);
    const booksRow = books.map((el, i) => {
      if (i > 4) {
        return;
      }
      return (
        <div className='block--slider-book' key={el.name} style={{'backgroundImage': `url(${el['thumbnailLink']})`}}>
          <div className='block--slider-book-hover'>
            <span>{el.name}</span>
            <NavLink to={'/book/' + el.id}>More</NavLink>
          </div>
        </div>
      )
    });
    return (
      <div className='block--slider'>
        <h1>{this.props.title}</h1>
        <div className='block--slider-wrapper'>
          <div className='block--slider-books'>
            {booksRow}
          </div>
        </div>
      </div>
    )
  }
}
