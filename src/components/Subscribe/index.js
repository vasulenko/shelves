import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

export class Subscribe extends Component {

  render() {
    return (
      <div className='row'>
        <div className='col-md-12'>
          <h1>{this.props.params.name}</h1>
        </div>
      </div>
    )
  }
}

function mapStateToProps() {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(Subscribe)
