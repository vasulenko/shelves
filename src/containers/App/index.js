import React, {Component} from 'react';
import Header from '../../components/Header';
import '../../default/base.css';
import '../App/style.css';
import Footer from "../../components/Footer";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as UserActions from "../../actions/UserActions";
import * as LibraryActions from "../../actions/LibraryActions";

export class App extends Component {
  componentWillMount() {
    this.props.actions.fetchAllAuthor();
    this.props.actions.fetchAllBooks();
    this.props.actions.fetchAllShelves();
    this.props.actions.fetchAllPublisher();
    this.props.actions.fetchAllSubscriptions();
    this.props.actions.fetchShelveTop();
    this.props.actions.fetchShelveNew();
    this.props.actions.checkCookie();
    //this.props.actions.fetchUserBooks();
  }

  render() {
    return (
      <div className='root'>
        <Header/>
        {this.props.children}
        <Footer/>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    library: state.library
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({...UserActions, ...LibraryActions}, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)