import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import '../BookPage/style.css';
import Slider from '../../components/Slider';
import * as LibraryActions from '../../actions/LibraryActions';
import * as UserActions from '../../actions/UserActions';

export class BookPage extends Component {
  componentWillMount() {
    this.props.actions.fetchUserBooks();
  }

  addToCart() {
    this.props.actions.addToCart(this.props.params.name);
  }

  downloadFullBook() {
    LibraryActions.downloadBookById(this.props.params.name);
  }

  render() {
    const id = this.props.params.name;
    const book = LibraryActions.getBookById(id);
    const author = LibraryActions.getAuthorById(book.authorId);
    const similar = LibraryActions.getShelveById(book.shelveId);
    const alreadyBuyedBooks = this.props.user.books || [];
    let buyed = false;
    alreadyBuyedBooks.forEach(book => {
      if (book.id == id) {
        buyed = true;
      }
    });
    let unavaliable = {bool: false, mess: ''};
    if (this.props.library.cart.indexOf(id) !== -1) {
      unavaliable.bool = true;
      unavaliable.mess = 'Already in cart';
    }
    return (
      <div className='block--book'>
        <div className='block--book-main'>
          <div className='block--book-img'>
            <img src={book.thumbnailLink} alt={book.name}/>
          </div>
          <div className='block--book-content'>
            <h1 className='block--book-title'>{book.name}</h1>
            <h2 className='block--book-price'>Price: ${book.price}</h2>
            <h2 className='block--book-author'>Author: {author.name}</h2>
            <h2 className='block--book-categories'>Shelve: {similar['_embedded'].shelveName}</h2>
            <a className='block--book-download' target='_blank' href={book.partFileLink}>Download part</a>
            {unavaliable.bool ? <p>{unavaliable.mess}</p>
              : buyed
                ? <button className='block--book-download' onClick={this.downloadFullBook.bind(this)}>Download
                  full</button>
                : <button className='block--book-download' onClick={this.addToCart.bind(this)}>Add to cart</button>}
          </div>
        </div>
        <div className='block--book-description'>{book.description}</div>
        <Slider books={similar['_embedded'].books} title={'Same as current'}/>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    library: state.library
  }
}

function mapDispatchToProps(dispatch) {
  return {actions: bindActionCreators({...LibraryActions, ...UserActions}, dispatch)}
}

export default connect(mapStateToProps, mapDispatchToProps)(BookPage)
