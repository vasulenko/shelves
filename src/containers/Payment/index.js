import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import '../LoginPage/style.css';
import * as LibraryActions from '../../actions/LibraryActions';


export class Payment extends Component {
  handleSubmit(e) {
    e.preventDefault();
    const id = this.props.library.order.id;
    const number = document.getElementById('number').value;
    const cvv = document.getElementById('cvv').value;
    const date = document.getElementById('date').value;
    const data = {
      number: number,
      cvv: cvv,
      date: date,
      id: id
    };
    this.props.actions.orderPay(data);
  }

  render() {
    return (
      <form className={'form'} onSubmit={this.handleSubmit.bind(this)}>
        <div className="input-field">
          <input id="number" placeholder={'Card number'} type={'text'} className="validate"/>
        </div>
        <div className={"input-field"}>
          <input id="cvv" placeholder={'CVV'} type={'password'} className="validate"/>
        </div>
        <div className={"input-field"}>
          <input id="date" placeholder={'Expires End'} type={'date'} className="validate"/>
        </div>
        <button className={'btn btn-primary red accent-2'} type='submit'>Pay</button>
      </form>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    library: state.library
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(LibraryActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Payment)
