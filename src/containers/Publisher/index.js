import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

export class Publisher extends Component {

  render() {
    return (
      <div className='row'>
        <div className='col-md-12'>
          <h1>Id: {this.props.params.id}</h1>
        </div>
      </div>
    )
  }
}

function mapStateToProps() {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(Publisher)
