import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import '../Report/style.css';
import * as LibraryActions from "../../actions/LibraryActions";

export class Report extends Component {

  fetchReport() {
    const start = new Date(document.getElementById('from').value).getTime();
    const end = new Date(document.getElementById('to').value).getTime();
    const type = document.getElementById('report_type').value;
    let helper = document.getElementById('report_helper').value;
    if (isNaN(start) || isNaN(end)) {
      return;
    }
    if (type === 'books' || type === 'subscriptions' || type === 'total') {
      helper = '';
    }
    this.props.actions.fetchReport(start, end, type, helper);
  }

  fetchReportToFile() {
    const start = new Date(document.getElementById('from').value).getTime();
    const end = new Date(document.getElementById('to').value).getTime();
    const type = document.getElementById('report_type').value;
    let helper = document.getElementById('report_helper').value;
    if (isNaN(start) || isNaN(end)) {
      return;
    }
    if (type === 'books' || type === 'subscriptions' || type === 'total') {
      helper = '';
    }
    const res = LibraryActions.fetchReportToFile(start, end, type, helper);
    console.log(res);
  }

  extendReportSearch() {
    console.log('in');
    let type = document.getElementById('report_type').value;
    console.log(type);
    if (type === 'author') {
      let helper = document.getElementById('report_helper');
      helper.innerHTML = '';
      this.props.library.authors.forEach(el => {
        let option = document.createElement('option');
        option.value = el.id;
        option.innerText = el.name;
        helper.appendChild(option);
      });
    }
    if (type === 'publisher') {
      let helper = document.getElementById('report_helper');
      helper.innerHTML = '';
      this.props.library.publisher.forEach(el => {
        let option = document.createElement('option');
        option.value = el.id;
        option.innerText = el.name;
        helper.appendChild(option);
      });
    }
    if (type === 'shelve') {
      let helper = document.getElementById('report_helper');
      helper.innerHTML = '';
      this.props.library.shelves.forEach(el => {
        let option = document.createElement('option');
        option.value = el.id;
        option.innerText = el.name;
        helper.appendChild(option);
      });
    }
    if (type === 'books' || type === 'subscriptions' || type === 'total') {
      let helper = document.getElementById('report_helper');
      helper.innerHTML = '';
    }
  }

  render() {
    const result = this.props.library.report['_embedded'] || [];
    const paymentRow = result.map(el => {
      const cellArr = Object.entries(el).map(cell => {
        return (
          <div className='block--report-result-row'>
            <div className="block--report-result-cell">{cell[0]}</div>
            <div className="block--report-result-cell">{cell[0] === 'date' ? new Date(cell[1]).getDay() + ' ' + new Date(cell[1]).getMonth() + ' ' + new Date(cell[1]).getFullYear(): cell[1]}</div>
          </div>
        )
      });
      return (
        <div className='block--report-result-entry'>
          {cellArr}
        </div>
      )
    });
    return (
      <div className='block--report'>
        <input type="date" name='from' id='from'/>
        <input type="date" name='to' id='to'/>
        <select name="type" id="report_type" onChange={this.extendReportSearch.bind(this)} className="browser-default">
          <option value="" disabled defaultValue>Choose your option</option>
          <option value="books">Books</option>
          <option value="subscriptions">Subscriptions</option>
          <option value="author">Author</option>
          <option value="shelve">Shelve</option>
          <option value="publisher">Publisher</option>
          <option value="total">Total</option>
        </select>

        <select name="type" id="report_helper" className="browser-default">
          <option value="" disabled defaultValue>Choose your option</option>
        </select>
        <div className='block--report-send_block'>
          <button onClick={this.fetchReport.bind(this)} className='block--report-send btn red accent-2'>Rush</button>
          <button onClick={this.fetchReportToFile.bind(this)} className='block--report-send btn red accent-2'>Download
          </button>
        </div>
        <div className='block--report-result'>
          {paymentRow}
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    library: state.library
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(LibraryActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Report)