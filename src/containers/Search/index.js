import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as LibraryActions from '../../actions/LibraryActions';
import NavLink from '../../components/NavLink';
import '../Search/style.css';

export class Search extends Component {
  search(e) {
    const data = document.getElementById('search-input').value;
    this.props.actions.searchBook(data);
  }

  render() {
    const books = this.props.library.search;
    const booksRow = books.map(el => {
      return (
        <div className='block--search-book' key={el.link} style={{'backgroundImage': `url(${el.thumbnailLink})`}}>
          <div className='block--search-book-hover'>
            <span>{el.name}</span>
            <NavLink to={'/book/' + el.id}>More</NavLink>
          </div>
        </div>
      )
    });
    return (
      <div className='block--search'>
        <div className='block--search-form'>
          <input id='search-input' type="text" className=''/>
          <button className={'btn red accent-2'} onClick={this.search.bind(this)}>Search</button>
        </div>
        <div className='block--search-result'>
          <div className='block--search-books'>
            {booksRow}
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    library: state.library
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(LibraryActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search)
