import React, {Component} from 'react';
import {connect} from 'react-redux';
import '../Shelve/style.css';
import NavLink from '../../components/NavLink';
import * as LibraryActions from '../../actions/LibraryActions';

export class Shelve extends Component {
  componentWillMount() {
  }

  render() {
    const id = this.props.params.name;
    const shelve = LibraryActions.getShelveById(id);
    const books = shelve['_embedded']['books'];
    const booksRow = books.map(el => {
      return (
        <div className='block--shelve-book' key={el.link} style={{'backgroundImage': `url(${el.thumbnailLink})`}}>
          <div className='block--shelve-book-hover'>
            <span>{el.name}</span>
            <NavLink to={'/book/' + el.id}>More</NavLink>
          </div>
        </div>
      )
    });
    return (
      <div className='block--shelve'>
        <h1>{shelve['_embedded'].shelveName}</h1>
        <div className='block--shelve-books'>
          {booksRow}
        </div>
      </div>
    )
  }
}

function mapStateToProps() {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(Shelve)
