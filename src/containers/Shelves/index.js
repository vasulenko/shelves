import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import NavLink from "../../components/NavLink";
import '../Shelves/style.css';
import * as LibraryActions from '../../actions/LibraryActions';

export class Shelves extends Component {
  componentWillMount() {
    const {shelves} = this.props.library;
    if (shelves.length < 1) {
      this.props.actions.fetchAllShelves();
    }
  }

  render() {
    const shelves = this.props.library.shelves || [];
    const shelvesRow = shelves.map(el => {
      return (
        <NavLink to={'/shelve/' + el.id} key={el.name} className={'block--shelves-shelve'}>{el.name}</NavLink>
      )
    });
    return (
      <div className='block--shelves'>
        {shelvesRow}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    library: state.library
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(LibraryActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Shelves)
