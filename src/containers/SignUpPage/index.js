import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import '../SignUpPage/style.css';
import * as UserActions from '../../actions/UserActions';

export class SignUpPage extends Component {
  handleSubmit(e) {
    e.preventDefault();
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    const name = document.getElementById('name').value;
    const data = {
      name: name,
      email: email,
      password:password
    };
    this.props.actions.signup(data);
  }

  render() {
    return (
      <form className={'form'} onSubmit={this.handleSubmit.bind(this)}>
        <div className="input-field">
          <input id="name" placeholder={'Name'} type={'text'} className="validate"/>
        </div>
        <div className="input-field">
          <input id="email" placeholder={'Email'} type={'email'} className="validate"/>
        </div>
        <div className={"input-field"}>
          <input id="password" placeholder={'Password'} type={'password'} className="validate"/>
        </div>
        <button className={'btn btn-primary red accent-2'} type='submit'>Sign in</button>
      </form>
    )
  }
}

function mapStateToProps() {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(UserActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUpPage)