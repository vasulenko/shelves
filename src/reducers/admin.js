import {ADD_NEW_SUBSCRIPTION, ADD_NEW_PUBLISHER, ADD_NEW_SHELVE, ADD_NEW_AUTHOR, ADD_NEW_BOOK} from '../constants/Admin'

const initialState = {};
export default function adminstate(state = initialState, action) {
  switch (action.type) {
    case ADD_NEW_AUTHOR:
      return {
        ...state
      };
    case ADD_NEW_BOOK:
      return {
        ...state
      };
    case ADD_NEW_SHELVE:
      return {
        ...state
      };
    case ADD_NEW_PUBLISHER:
      return {
        ...state
      };
    case ADD_NEW_SUBSCRIPTION:
      return {
        ...state
      };
    default:
      return state
  }
}
