import {combineReducers} from 'redux';
import user from './user';
import library from './library';
import admin from './admin';

export default combineReducers({
  user,
  library,
  admin
});
