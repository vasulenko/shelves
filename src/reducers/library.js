import {REMOVE_FROM_CART, ADD_TO_CART, REPORT} from '../constants/Library';
import {FETCH_ALL_BOOKS, FETCH_BOOK_BY_ID, SEARCH_BOOK} from "../constants/Book";
import {FETCH_ALL_AUTHORS, FETCH_AUTHOR_BY_ID} from "../constants/Author";
import {
  FETCH_ALL_SHELVES,
  FETCH_SHELVE_BY_ID,
  FETCH_SHELVE_TOP,
  FETCH_SHELVE_NEW
} from "../constants/Shelves";
import {FETCH_ALL_PUBLISHERS, FETCH_PUBLISHER_BY_ID} from "../constants/Publisher";
import {FETCH_ALL_SUBSCRIPTION, FETCH_SUBSCRIPTION_BY_ID} from "../constants/Subscription";
import {ORDER_INIT, ORDER_PAY} from "../constants/Order";

let initialState = {
  books: [],
  book: {},
  authors: [],
  author: {},
  shelve: {},
  shelves: [],
  top: [],
  new: [],
  subscriptions: [],
  subscription: {},
  publisher: [],
  cart: [],
  report: {},
  order: {},
  search: []
};

export default function page(state = initialState, action) {
  switch (action.type) {
    case ORDER_INIT:
      return {...state, order: action.payload};
    case SEARCH_BOOK:
      return {...state, search: action.payload};
    case ORDER_PAY:
      return {...state, order: action.payload};
    case FETCH_BOOK_BY_ID:
      return {...state, book: action.payload};
    case FETCH_ALL_BOOKS:
      return {...state, books: action.payload};
    case FETCH_ALL_AUTHORS:
      return {...state, authors: action.payload};
    case FETCH_AUTHOR_BY_ID:
      return {...state, author: action.payload};
    case FETCH_SHELVE_BY_ID:
      return {...state, shelve: action.payload};
    case REPORT:
      return {...state, report: action.payload};
    case FETCH_SHELVE_TOP:
      return {...state, top: action.payload};
    case FETCH_SHELVE_NEW:
      return {...state, new: action.payload};
    case FETCH_ALL_SHELVES:
      return {...state, shelves: action.payload};
    case FETCH_ALL_PUBLISHERS:
      return {...state, publisher: action.payload};
    case FETCH_PUBLISHER_BY_ID:
      return {...state, author: action.payload};
    case FETCH_ALL_SUBSCRIPTION:
      return {...state, subscriptions: action.payload};
    case FETCH_SUBSCRIPTION_BY_ID:
      return {...state, subscription: action.payload};
    case ADD_TO_CART:
      return {...state, subscriptions: action.payload};
    case REMOVE_FROM_CART:
      return {...state, subscription: action.payload};
    default:
      return state;
  }
}