import {
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  SIGNUP_FAIL,
  SIGNUP_SUCCESS,
  LOGOUT,
  FETCH_USER,
  FETCH_USER_BOOKS
} from '../constants/User';

const initialState = {
  token: '',
  role: '',
  name: '',
  bookAvailable: false,
  isAuthenticated: false,
  books: []
};
export default function userstate(state = initialState, action) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        token: action.payload.token,
        role: action.payload.role,
        name: action.payload.name,
        bookAvailable: action.payload.bookAvailable,
        isAuthenticated: action.payload.isAuthenticated,
        books: action.payload.books
      };
    case FETCH_USER:
      return {
        ...state,
        bookAvailable: action.payload.bookAvailable,
        name: action.payload.name,
        id: action.payload.id
      };
    case FETCH_USER_BOOKS:
      return {
        ...state,
        books: action.payload.books
      };
    case LOGOUT:
      return {
        ...state,
        token: '',
        name: '',
        role: '',
        bookAvailable: [],
        isAuthenticated: false
      };
    case LOGIN_FAIL:
      return {
        ...state, name: '', isAuthenticated: false
      };
    case SIGNUP_SUCCESS:
      return {
        ...state,
        role: action.payload.role,
        token: action.payload.token,
        bookAvailable: [],
        isAuthenticated: action.payload.isAuthenticated
      };

    case SIGNUP_FAIL:
      return {
        ...state, token: '', isAuthenticated: false
      };
    default:
      return state
  }
}
