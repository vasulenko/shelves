import React from 'react'
import {Route, IndexRoute} from 'react-router'
import App from './containers/App'
import Admin from './components/Admin'
import Account from './components/Account'
import Home from './components/Home'
import Cart from './components/Cart'
import Subscribe from './components/Subscribe'
import LoginPage from './containers/LoginPage'
import BookPage from './containers/BookPage'
import Shelves from './containers/Shelves'
import Shelve from './containers/Shelve'
import Search from './containers/Search'
import Publisher from './containers/Publisher'
import SignUpPage from './containers/SignUpPage'
import NotFound from './components/NotFound'
import Report from "./containers/Report";
import Payment from "./containers/Payment";

export const routes = (
  <div>
    <Route path='/' component={App}>
      <IndexRoute component={Home}/>
      <Route path='/admin' component={Admin}/>
      <Route path='/me' component={Account}/>
      <Route path='/sign-in' component={LoginPage}/>
      <Route path='/sign-up' component={SignUpPage}/>
      <Route path='/book/:name' component={BookPage}/>
      <Route path='/subscribe' component={Subscribe}/>
      <Route path='/shelves' component={Shelves}/>
      <Route path='/search' component={Search}/>
      <Route path='/shelve/:name' component={Shelve}/>
      <Route path='/cart' component={Cart}/>
      <Route path='/payment' component={Payment}/>
      <Route path='/report' component={Report}/>
      <Route path='/publisher/:id' component={Publisher}/>
      <Route path='*' component={NotFound}/>
    </Route>
  </div>
);